
public class SignUpService {
	private final PasswordsRepository passwordsRepository;

	public SignUpService(PasswordsRepository passwordsRepository) {
		this.passwordsRepository = passwordsRepository;
	}

	public boolean isPasswordInBlackList(String pass){
		return passwordsRepository.checkPasswordInBlackList(pass);
	}
}
