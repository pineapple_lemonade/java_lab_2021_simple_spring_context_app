import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Main {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
		SignUpService signUpService = context.getBean(SignUpService.class);
		System.out.println(signUpService.isPasswordInBlackList("qwerty007"));
		System.out.println(signUpService.isPasswordInBlackList("qwerty008"));
	}
}
