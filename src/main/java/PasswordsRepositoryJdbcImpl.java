import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import javax.sql.DataSource;
import java.util.*;

public class PasswordsRepositoryJdbcImpl implements PasswordsRepository {

	private final JdbcTemplate jdbcTemplate;

	//language SQL
	private static final String SQL_SELECT_BY_PASSWORD = "select * from passwords_black_list where password = ?";

	//language SQL
	private static final String SQL_DELETE_PASSWORD = "delete from passwords_black_list where password = ?";

	//language SQL
	private static final String SQL_INSERT_PASSWORD = "insert into passwords_black_list(password) values (?)";

	private final ResultSetExtractor<List<String>> passwordsResultSet = resultSet ->{
		List<String> result = new ArrayList<>();
		while (resultSet.next()){
			result.add(resultSet.getString("password"));
		}
		return result;
	};

	@Override
	public boolean checkPasswordInBlackList(String password) {
		return !Objects.requireNonNull(jdbcTemplate.query(SQL_SELECT_BY_PASSWORD, passwordsResultSet, password)).isEmpty();
	}

	@Override
	public void savePasswordInBlackList(String password) {
		jdbcTemplate.update(SQL_INSERT_PASSWORD,password);
	}

	@Override
	public void deletePasswordFromBlackList(String password) {
		jdbcTemplate.update(SQL_DELETE_PASSWORD,password);
	}

	public PasswordsRepositoryJdbcImpl(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
}
