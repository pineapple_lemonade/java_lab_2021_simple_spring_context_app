import java.util.List;

public interface PasswordsRepository {
	boolean checkPasswordInBlackList(String password);
	void savePasswordInBlackList(String password);
	void deletePasswordFromBlackList(String password);
}
